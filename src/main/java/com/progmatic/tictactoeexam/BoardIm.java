/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.List;

/**
 *
 * @author bodnaristvan
 */
public class BoardIm implements Board{
    private Cell actCell;

    public BoardIm(Cell actCell) {
        this.actCell = actCell;
    }

    public Cell getActCell() {
        return actCell;
    }
    
    
    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        rowIdx = actCell.getRow();
        colIdx = actCell.getCol();
        if (rowIdx < 0 && rowIdx > 2 && colIdx < 0 && colIdx > 2) {
            String message = "Index not on the board";
            throw new CellException(rowIdx, colIdx, message);
        }
        return actCell.getCellsPlayer();
    }

    @Override
    public void put(Cell cell) throws CellException {
         int rowIdx = cell.getRow();
        int colIdx = cell.getCol();
        if (rowIdx != 0 || rowIdx != 1 || rowIdx != 2 || colIdx != 0 || colIdx != 1 || colIdx != 2) {
            String message = "Index not on the board";
            throw new CellException(cell.getRow(), cell.getCol(), message);
        }
        if (cell.getCellsPlayer() != PlayerType.EMPTY) {
            String message = "Cell is reserved";
            throw new CellException(cell.getRow(), cell.getCol(), message);
        }
        PlayerType actPlTy = cell.getCellsPlayer();
    }

    @Override
    public boolean hasWon(PlayerType p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cell> emptyCells() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
